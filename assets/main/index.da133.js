System.register("chunks:///_virtual/DemoScript.ts",["./_rollupPluginModLoBabelHelpers.js","cc"],(function(t){"use strict";var e,o,r,n;return{setters:[function(t){e=t.inheritsLoose},function(t){o=t.cclegacy,r=t._decorator,n=t.Component}],execute:function(){var c;o._RF.push({},"6c8d8rHXDhFma9pL+TfU5sJ","DemoScript",void 0);var i=r.ccclass;r.property,t("DemoScript",i("DemoScript")(c=function(t){function o(){return t.apply(this,arguments)||this}return e(o,t),o.prototype.start=function(){window.CC_WECHAT&&wx.getOpenDataContext().postMessage({type:"give"})},o}(n))||c);o._RF.pop()}}}));

System.register("chunks:///_virtual/main",["./DemoScript.ts"],(function(){"use strict";return{setters:[null],execute:function(){}}}));

(function(r) {
  r('virtual:///prerequisite-imports/main', 'chunks:///_virtual/main'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});