let dataDemo = {
    data: [
        /*
        {
            rankScore: 0,
            avatarUrl: '',
            nickname: '',
        },
        */
    ],
};

// const maxCount = 3;
// for (let i = 0; i < maxCount; ++i) {
//     let item = {};
//     item.openid = Math.floor((Math.random() * 500));
//     item.avatarUrl = 'openDataContext/render/avatar.png';
//     item.nickname = 'Player_' + i;
//     dataDemo.data.push(item);
// }
// dataDemo.data.sort((a, b) => b.rankScore - a.rankScore);

dataDemo.getData = function resultData() {
    return new Promise(function(resolve, reject) {
        wx.getFriendCloudStorage({
            keyList: ['score'],
            success: function(res) {

                console.log('--success friends res:', res);
                dataDemo.data = []
                if (res.data != null && res.data.length > 0) {
                    for (let j = 0; j < res.data.length; j++) {
                        let item = {};
                        item.avatarUrl = res.data[j].avatarUrl
                        item.nickname = res.data[j].nickname
                        item.openid = res.data[j].openid
                        dataDemo.data.push(item);
                    }
                }
                resolve()
            },
            fail: function(res) {
                console.log('--fail friends res:', res);
                resolve()
            },
            complete: function(res) {
                console.log('--complete friends res:', res);

            },
        })
    })
}

// wx.getUserCloudStorageKeys({
//     success: function(res) {
//         console.log('--success getUserCloudStorageKeys res:', res);

//     },
//     fail: function(res) {
//         console.log('--fail friends res:', res);
//     },
//     complete: function(res) {
//         console.log('--complete friends res:', res);

//     },
// })

export default dataDemo;
