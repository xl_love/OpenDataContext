export default {
    container: {
        width: '100%',
        height: '100%',
        borderRadius: 12,
    },

    // header: {
    //     width: '100%',
    //     height: '10%',
    //     flexDirection: 'column',
    //     alignItems: 'center',
    //     backgroundColor: '#ffffff',
    //     borderBottomWidth: 0.5,
    //     borderColor: 'rgba(0, 0, 0, 0.3)',
    // },

    // title: {
    //     width: '10%',
    //     height: '100%',
    //     fontSize: 35,
    //     lineHeight: 80,
    //     textAlign: 'center',
    //     fontWeight: 'bold',
    //     borderBottomWidth: 6,
    //     borderColor: '#000000',
    // },

    // rankList: {
    //     width: '100%',
    //     height: '90%',
    //     backgroundColor: '#ffffff',
    // },

    array: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        marginTop: 10,
        borderRadius: 12,

    },

    // listTips: {
    //     width: '100%',
    //     height: '12%',
    //     lineHeight: '12%',
    //     textAlign: 'center',
    //     fontSize: 25,
    //     color: 'rgba(0,0,0,0.5)',
    //     backgroundColor: '#ffffff',
    //     borderRadius: 10,
    //     borderWidth: 1,
    //     borderColor: 'rgba(0, 0, 0, 1)',
    // },

    listItem: {
        backgroundColor: '#F7F7F7',
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    listItemOld: {
        backgroundColor: '#ffffff',
    },

    listItemUserData: {
        width: '75%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        // backgroundColor: '#cccccc',
    },

    listItemButton: {
        width: '25%',
        height: 100,
        fontSize: 33,
        fontWeight: 'bold',
        // paddingRight: 200,
        lineHeight: 100,
        textAlign: 'center',
        // backgroundColor: '#117700',
    },

    listItemNum: {
        width: 100,
        height: 80,
        fontSize: 30,
        fontWeight: 'bold',
        color: '#452E27',
        lineHeight: 100,
        textAlign: 'center',
    },

    listHeadImg: {
        borderRadius: 6,
        width: 70,
        height: 70,
    },

    listItemName: {
        width: 210,
        height: 100,
        fontSize: 30,
        lineHeight: 100,
        marginLeft: 30,
    },
}
