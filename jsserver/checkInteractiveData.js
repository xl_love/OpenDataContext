exports.main = function(arg) {
    try {
        arg = JSON.parse(arg)
        console.log('arg:', arg)
        const myOpenid = wx.getOpenId()
        console.log('myOpenid:', myOpenid)
        const toOpenid = arg.toUser
        console.log('toOpenid:', toOpenid)
        const opNum = arg.opNum
        console.log('opNum:', opNum)
        const now = new Date()
        const giftStorageKey = now.toDateString()
        const friendsStorage = wx.getFriendUserStorage([giftStorageKey])
        const userList = friendsStorage.user_item
        let ok = true

        // 用户每天只能给同一个好友赠送一次精力,每天最多送5次
        // const friendData = userList.find(userItem => userItem.openid === toOpenid)
        // const myData = userList.find(userItem => userItem.openid === myOpenid)
        // if (friendData) {
        //     const friendKV = friendData.kv_list[friendData.kv_list.length - 1]
        //     const selfKV = myData.kv_list[myData.kv_list.length - 1]
        //     let friendGift = friendKV && friendKV.value
        //     let selfGift = selfKV && selfKV.value
        //     if (friendGift) {
        //         friendGift = JSON.parse(friendGift)
        //     } else {
        //         friendGift = {
        //             receiveRecords: [],
        //             sendCount: 0
        //         }
        //     }
        //     if (selfGift) {
        //         selfGift = JSON.parse(selfGift)
        //     } else {
        //         selfGift = {
        //             receiveRecords: [],
        //             sendCount: 0
        //         }
        //     }

        //     // 精力重复送给同一个人
        //     const giftToSameOne = friendGift && friendGift.receiveRecords.some(item => {
        //             return item.fromOpenid === myOpenid
        //         })
        //         // 赠送次数超过限制
        //     const outLimit = selfGift && selfGift.sendCount >= 1
        //     const canNotGift = giftToSameOne || outLimit
        //         // 验证
        //     if (!canNotGift) {
        //         friendGift.receiveRecords.push({
        //             fromOpenid: myOpenid,
        //             time: Date.now()
        //         })
        //         selfGift.sendCount = selfGift.sendCount + 1
        //             // 写对方的数据
        //         let ret1 = wx.setFriendUserStorage(toOpenid, [{
        //                 key: giftStorageKey,
        //                 value: JSON.stringify(friendGift)
        //             }])
        //             // 写自己的数据
        //         let ret2 = wx.setFriendUserStorage(myOpenid, [{
        //             key: giftStorageKey,
        //             value: JSON.stringify(selfGift)
        //         }])
        //         if (ret1.errcode == 0 && ret2.errcode == 0) {
        //             ok = true
        //         } else {
        //             console.error('fail')
        //         }
        //     }
        // }

        if (ok) {
            // 验证通过
            return JSON.stringify({ "ret": true });
        } else {
            // 验证不通过
            return JSON.stringify({ "ret": false });
        }
    } catch (err) {
        console.error(err.message)
    }
}